package com.ecal.google.search;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		format={"html:target/html/"},
		features={"src/integration-test"}
	)
public class ITAllTest {

}
