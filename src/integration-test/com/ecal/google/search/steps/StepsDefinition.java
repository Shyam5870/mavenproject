package com.ecal.google.search.steps;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ecal.utils.PropertyFileLoader;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class StepsDefinition {
	private static WebDriver driver = null;
	private static PropertiesConfiguration config = null;
	
	static {
		config = PropertyFileLoader.config;

        if (config.getString("BROWSER").equalsIgnoreCase("FIREFOX")) {
        	driver = new FirefoxDriver();	
        }
        else if (config.getString("BROWSER").equalsIgnoreCase("CHROME")) {
        	System.setProperty("webdriver.chrome.driver", config.getString("DRIVER_LOCATION") + File.separatorChar + "chromedriver.exe");
        	driver = new ChromeDriver();	
        }
        else if (config.getString("BROWSER").equalsIgnoreCase("IE")) {
        	System.setProperty("webdriver.ie.driver", config.getString("DRIVER_LOCATION") + File.separatorChar + "IEDriverServer.exe");
        	driver = new InternetExplorerDriver();
        }
        
        driver.manage().timeouts().implicitlyWait(config.getInt("PAGE_TIMEOUT"), TimeUnit.SECONDS);
	}
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		driver.get(config.getString("HOME_PAGE"));
	}

	@When("^User search for \"([^\"]*)\"$")
	public void user_search_the_google(String key) throws Throwable {
		WebElement searchBox = new WebDriverWait(driver, config.getInt("PAGE_TIMEOUT"))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(config.getString("HOME_PAGE_SEARCH_FIELD"))));
		searchBox.sendKeys(key);
		
		WebElement searchBtn = new WebDriverWait(driver, config.getInt("PAGE_TIMEOUT"))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(config.getString("HOME_PAGE_SEARCH_BUTTON"))));
		searchBtn.click();
	}

	@Then("^google should display results about \"([^\"]*)\"$")
	public void google_should_display_results_about(String expected) throws Throwable {
		WebElement actual = new WebDriverWait(driver, config.getInt("PAGE_TIMEOUT"))
			.until(ExpectedConditions.presenceOfElementLocated(By.xpath(config.getString("HOME_PAGE_SEARCH_RESULT"))));
		
		Assert.assertTrue(actual.getText().contains(expected));
	}
	
	@Then("^this scenario should fail$")
	public void this_scenario_should_fail() throws Throwable {
	    System.out.println("This scenario should fail");
	    Assert.assertTrue(false);
	}

}
